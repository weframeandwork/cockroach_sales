package test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Pages.NewAccountsPage;
import Pages.AccountPage;
import Pages.HomePage;
import Pages.NewAccountsPage;
import base.TestBase;
import util.Utilities;

public class NewAccountsPageTest extends TestBase{

	HomePage homePage;
	NewAccountsPage newAccountsPage;
	Utilities util;
	AccountPage accountPage;
	
	public NewAccountsPageTest() throws IOException {
		super();
		
	}

	@BeforeMethod
	public void setUp() throws IOException, InterruptedException {
		
		initialize();
		homePage = validLogin();
		Thread.sleep(10000);
		util = new Utilities();

		
	}
	
	@AfterMethod
	public void tearDown() {
		
		driver.quit();
	}
	
	// Verify user can enter Account Name - Test 16
	@Test
	public void verifyAccountNameText() throws IOException {
		
		homePage.clickOnAccountsDropdown();
		newAccountsPage = homePage.clickOnNewAccountLink();
		
		newAccountsPage.enterAccountName(prop.getProperty("accountHolderName"));
		accountPage = newAccountsPage.clickOnSave();
		
	}
		
	
}
