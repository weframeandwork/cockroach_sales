package test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import Pages.LoginPage;
import base.TestBase;

public class LoginTest extends TestBase {

	public LoginPage loginpg;

	public LoginTest() throws IOException {

		super();
	}

	@BeforeSuite
	public void setup() {

		initialize();
		try {
			loginpg = new LoginPage(driver);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test(enabled = false)
	public void login() {

		driver.findElement(By.id("username")).sendKeys(prop.getProperty("username"));
		driver.findElement(By.id("password")).sendKeys(prop.getProperty("password"));
		driver.findElement(By.id("Login")).click();
		Assert.assertEquals(driver.getTitle(), "Home | Salesforce");
	}

	@Test(priority=1) // Test for remember me functionality (Checking Remember me should remember the
			// password entered)

	public void rememberMeCheckedTest() throws InterruptedException {
		loginpg.clickRememberMe();
		loginpg.doLogin(prop.getProperty("username"), prop.getProperty("password"));
		driver.navigate().to(prop.getProperty("url"));
		Thread.sleep(5000);
		String rememText = loginpg.getElementText(By.id("username"));
		Assert.assertEquals(rememText, prop.getProperty("username"), "Email id not remembered");
	}

	@Test(priority=2)// Test for remember me functionality (Unchecking Remember me must not remember
			// the password)
	public void rememberMeUncheckedTest() throws InterruptedException {
		loginpg.clickRememberMe();
		loginpg.doLogin(prop.getProperty("username"), prop.getProperty("password"));
		driver.navigate().to(prop.getProperty("url"));
		String rememText = loginpg.getElementText(By.id("username"));
		System.out.println("RememText is " + rememText);
		Assert.assertEquals(rememText, "");
		
	}
	@Test(priority=3)// Test for Forgot Password Functionality
		public void forgotPasswordTest(){
		loginpg.forgotPassword();
		//capturing the forgotpassword msg
		WebElement msg= driver.findElement(By.className("mb12"));	
		String forgotmsg=msg.getText();
		System.out.println("Forgot your passowrd message is " + forgotmsg);
		Assert.assertEquals(forgotmsg,"Forgot Your Password");
		driver.findElement(By.xpath("//*[@id='forgotPassForm']/div[1]/a")).click();
				
	}
	@Test(priority=4)//Test for Freetrail page.
	
	public void TryforFreeTest(){
		
		loginpg.TryforFree();
		WebElement freemsg= driver.findElement(By.className("header-text"));
		String freetrailmsg=freemsg.getText();
		System.out.println("Freetrail text is " + freetrailmsg);
		Assert.assertEquals(freetrailmsg,"Get Your FREE 30-Days Trial Now!");
		}
	
	@AfterSuite
	public void tearDown() {
		driver.quit();
	}
}
