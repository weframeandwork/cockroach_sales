package util;
import java.io.IOException;
import org.openqa.selenium.Alert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import base.TestBase;

public class Utilities extends TestBase {

	public Utilities() throws IOException {
		super();
		
	}

	public boolean isAlertPresent() {
		
		WebDriverWait wait = new WebDriverWait(driver,WAIT_TIME);
		
		Alert alert = wait.until(ExpectedConditions.alertIsPresent());
		
		if(alert != null) {
			return true;
		}
		else 
			return false;
	
	}
}
