package Pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import base.TestBase;

//This is the page class to implement the POM. All the controls of the page are listed here and called when necessary.
public class LoginPage extends TestBase {

	private WebDriver driver;

	public LoginPage(WebDriver driver) throws IOException {
		super();
		this.driver = driver; // This is for getting the driver from the TestBase to login page class
		// TODO Auto-generated constructor stub
	}

	// Below is the OR, object repository. All the elements/objects of the page are
	// listed here
	By username = By.id("username");
	By password = By.id("password");
	By loginBtn = By.id("Login");
	By errorMsg = By.xpath("//*[@id='error']");
	By rememberMe = By.xpath("//input[@name='rememberUn']");
	By forgotPwd = By.id("forgot_password_link");
	By custDomain = By.id("mydomainLink");
	By privacyLink = By.id("privacy-link");
	By Tryforfreelink=By.id("signup_link");

	public String verifyTitle() {
		return driver.getTitle();
	}
		public void doLogin(String usrname, String pwd) {
		driver.findElement(username).sendKeys(usrname);
		driver.findElement(password).sendKeys(pwd);
		driver.findElement(loginBtn).click();
	}
		
		public void clickRememberMe() {
			driver.findElement(rememberMe).click();
		}
		
		public void forgotPassword(){
			
			driver.findElement(forgotPwd).click();
		}
		
		public String getElementText(By element) {
			return driver.findElement(element).getAttribute("value");
		}
		public void TryforFree() {

			driver.findElement(Tryforfreelink).click();
			
		}
		
}
