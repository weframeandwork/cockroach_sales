package Pages;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import base.TestBase;

public class NewAccountsPage extends TestBase{

	public NewAccountsPage() throws IOException {
		
	}
	
	
	WebElement accountName = driver.findElement(By.xpath("//span[text()='Account Name']/parent::label/following-sibling::div//input"));
	By typeDropDown = By.xpath("//span[text()='Type']/parent::span/following-sibling::div//a");
	By dropDownList = By.xpath("//div[@class='select-options']/ul/li");
	By saveButton = By.xpath("//button[@title='Save']");
	By accountHolderName = By.xpath("//div[@class='windowViewMode-normal oneContent active lafPageHost']//span[@class='custom-truncate uiOutputText']");
	WebElement newAccountText = driver.findElement(By.xpath("//h2[text()='New Account']"));
	
	
	public AccountPage clickOnSave() throws IOException {
		driver.findElement(saveButton).click();
		return new AccountPage();
	}
	public void enterAccountName(String name) {
		WebDriverWait wait = new WebDriverWait(driver,WAIT_TIME);
		wait.until(ExpectedConditions.visibilityOf(newAccountText));
		
		accountName.click();
		accountName.sendKeys(name);
	}
	public void selectValueFromTypeDropDown(String jobType) {
		driver.findElement(typeDropDown).click();
		
		Iterator<WebElement> it = driver.findElements(dropDownList).iterator();
		while(it.hasNext()) {
			if(it.next().getText().equalsIgnoreCase(jobType)) {
				
				it.next().click();
				break;
			}
		}
	
	}
	public String verifyAccountHolder() {
		
		return driver.findElement(accountHolderName).getText();
	}

}
