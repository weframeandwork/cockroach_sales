package Pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import base.TestBase;

public class HomePage extends TestBase{

	public HomePage() throws IOException {
		
		
	}
	
	By accountsDropdown = By.xpath("//a[@title='Accounts']/following-sibling::one-app-nav-bar-item-dropdown//a");
	By newAccountLink = By.xpath("//span[text()='New Account']");
	
	
	public void clickOnAccountsDropdown() {
		
		driver.findElement(accountsDropdown).click();
		
	}
	
	public NewAccountsPage clickOnNewAccountLink() throws IOException {
		
		WebElement newAccount = driver.findElement(newAccountLink);
		WebDriverWait wait = new WebDriverWait(driver,WAIT_TIME);
		wait.until(ExpectedConditions.elementToBeClickable(newAccount));
		
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();",newAccount);
		
		return new NewAccountsPage();
	}

}
